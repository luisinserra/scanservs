package br.com.operon.shell.service;

import org.springframework.web.bind.annotation.*;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class ShellEndPoints {

    @Autowired
    ShellService s;

    @GetMapping("/getServicos")
    public String getServicos() {

        String parm = "ls";
        String resposta = s.execCommand(parm);
        return resposta;
    }
}