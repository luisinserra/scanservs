package br.com.operon.shell;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import br.com.operon.shell.service.ShellService;

@SpringBootApplication
@RestController
public class DemoApplication {

	@Autowired
    ShellService s;

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/getListaServicos")
    public String getServicos() {

		ShellService s = new ShellService();
        String parm = "ls";
        String resposta = s.execCommand(parm);
        return resposta;
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}